#![feature(test)]

extern crate test;

const SOLDIERS: u8 = 12;
type Move = [u8; 3]; // Number of soldiers per battlefield

struct Strategy {
    regret_sum: Vec<i64>,
    strategy: Vec<f64>,
    strategy_sum: Vec<f64>,
}

fn sample() -> f64 {
    use rand::{distributions::Uniform, thread_rng, Rng};
    thread_rng().sample(Uniform::from(0.0..1.0))
}

impl Strategy {
    fn new(n_moves: usize) -> Self {
        let mut regrets = Vec::with_capacity(n_moves);
        let mut strategy = Vec::with_capacity(n_moves);
        let mut strategy_sum = Vec::with_capacity(n_moves);
        for _ in 0..n_moves {
            regrets.push(0);
            strategy.push(1.0 / n_moves as f64);
            strategy_sum.push(0.0);
        }
        Self {
            regret_sum: regrets,
            strategy: strategy,
            strategy_sum: strategy_sum,
        }
    }

    fn sample(&self) -> usize {
        let s = self.strategy.iter().sum::<f64>();
        assert!((s - 1.0).abs() < 0.002);
        let threshold = sample();
        let mut sum = 0.0;
        let mut action = 0;
        for v in self.strategy.iter() {
            sum += v;
            if sum > threshold {
                break;
            }
            action += 1;
        }
        action
    }


    fn match_regrets(&mut self) {
        let sum = self.regret_sum.iter().filter(|v| *v > &0).sum::<i64>() as f64;
        let n_moves = self.strategy.len();
        self.strategy
            .iter_mut()
            .zip(self.regret_sum.iter())
            .for_each(|(s, r)| {
                *s = if sum == 0.0 {
                    1.0 / n_moves as f64
                } else {
                    if r > &0 {
                        (*r as f64) / sum
                    } else {
                        0.0
                    }
                };
            });
        self.strategy_sum
            .iter_mut()
            .zip(self.strategy.iter())
            .for_each(|(s_sum, s)| {
                *s_sum += s;
            });
    }

    fn average_strategy(&mut self) {
        let sum: f64 = self.strategy_sum.iter().sum();
        let n_moves = self.strategy.len();
        self.strategy
            .iter_mut()
            .zip(self.strategy_sum.iter())
            .for_each(|(a, s)| {
                if sum > 0.0 {
                    *a = s / sum;
                } else {
                    *a = 1.0 / n_moves as f64;
                }
            });
    }
}


fn update_regrets(reward_table: &Vec<i64>, n_moves: usize, p1: &mut Strategy, player: usize, opponent: usize) {
    let action_utility = reward_table[opponent * n_moves + player];
    for i in 0..n_moves {
        if i == player {
            continue;
        }
        let alternative_utility = reward_table[opponent * n_moves + i];
        p1.regret_sum[i] += alternative_utility - action_utility;
    }
}

fn train(p1: &mut Strategy, p2: &mut Strategy, n_rounds: u64, moves: &Vec<Move>, reward_table: &Vec<i64>, train: bool, print: bool) {
    let n_moves = moves.len();
    for _ in 0..n_rounds {
        let p1_action = p1.sample();
        let p2_action = p2.sample();

        if print {
            let p1_move = moves[p1.sample()];
            let p2_move = moves[p2.sample()];

            println!("Player1: {:?}", p1_move);
            println!("Player2: {:?}", p2_move);

            println!("Reward: {}", move_reward(&p1_move, &p2_move));
            println!("Reward: {}", move_reward(&p2_move, &p1_move));
        }

        if train {
            update_regrets(reward_table, n_moves, p1, p1_action, p2_action);
            update_regrets(reward_table, n_moves, p2, p2_action, p1_action);
            p1.match_regrets();
            p2.match_regrets();
        }
    }
}

fn move_reward(player: &Move, opponent: &Move) -> i64 {
    let won_battles = |p1: &Move, p2: &Move| -> u64 {
        p1.iter()
            .zip(p2.iter())
            .map(|(a1, a2)| if a1 > a2 { 1 } else { 0 })
            .sum()
    };

    let p1_won = won_battles(&player, &opponent) as i64;
    let p2_won = won_battles(&opponent, &player) as i64;

    p1_won - p2_won
}

fn print_strategy(moves: &Vec<Move>, s: &Strategy) {
    let mut p: Vec<_> = s.strategy.iter().zip(s.regret_sum.iter()).enumerate().collect();
    p.sort_by(|(_, (p1, _)), (_, (p2, _))| p2.partial_cmp(p1).unwrap());
    for (move_idx, (prob, regret)) in p {
        let play = moves[move_idx];
        if play.iter().sum::<u8>() == SOLDIERS {
            println!("Play {:?} with probability {:.05} and regret {}", play, prob, regret);
        }
    }
}

fn main() {
    let mut moves: Vec<Move> = Vec::new();
    for i in 0..=SOLDIERS {
        for j in 0..=SOLDIERS {
            for k in 0..=SOLDIERS  {
                if i + j + k == SOLDIERS && i <= j && j <= k {
                    moves.push([i, j, k]);
                }
            }
        }
    }
    println!("Moves: {:?}", moves);
    let mut reward_table = Vec::new();
    for m1 in moves.iter() {
        for m2 in moves.iter() {
            reward_table.push(move_reward(&m2, &m1));
        }
    }

    let mut p1 = Strategy::new(moves.len());
    let mut p2 = Strategy::new(moves.len());

    train(&mut p1, &mut p2, 1_000_000, &moves, &reward_table, true, false);

    p1.average_strategy();
    p2.average_strategy();

    println!("Player 1:");
    print_strategy(&moves, &p1);
    println!("Player 2:");
    print_strategy(&moves, &p2);
}


#[cfg(test)]
mod tests {
    use super::*;
    use test::Bencher;

    #[bench]
    fn bench_train(b: &mut Bencher) {
        let mut moves: Vec<Move> = Vec::new();
        for i in 0..=SOLDIERS {
            for j in 0..=SOLDIERS {
                for k in 0..=SOLDIERS  {
                    if i + j + k == SOLDIERS && i <= j && j <= k {
                        moves.push([i, j, k]);
                    }
                }
            }
        }
        println!("Moves: {:?}", moves);
        let mut reward_table = Vec::new();
        for m1 in moves.iter() {
            for m2 in moves.iter() {
                reward_table.push(move_reward(&m2, &m1));
            }
        }

        let mut p1 = Strategy::new(moves.len());
        let mut p2 = Strategy::new(moves.len());

        b.iter(|| {
            train(&mut p1, &mut p2, 1_000_000, &moves, &reward_table, true, false);
        });

        p1.average_strategy();
        p2.average_strategy();

        println!("Player 1:");
        print_strategy(&moves, &p1);
        println!("Player 2:");
        print_strategy(&moves, &p2);
    }
}
